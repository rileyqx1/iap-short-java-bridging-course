# Lesson 1: From Python to Java

## Learning Objectives

By the end of this lesson, students should be able to:
- compare Python and Java basic data types and operators
- compile and run Java code
- create class and methods in Java
- create methods with arguments in Java
- use logical and relational operators in if-else statements

## Text Book

I am using the following text for this short notes.

*Introduction to Java Programming: Comprehensive Version*, 9th Edition, by Daniel Liang.

## Python's and Java's Hello ISTD

This is why we love Python.

```python
# python

print("Hello ISTD!")
```

But this is why we admire Java.

```java
// java

public class Hello{
    public static void main(String[] args){
        System.out.println("Hello ISTD!");
    }
}
```

- In Java, you must always create a `class`. For your main program, that class must be `public`, which means it can be accessed by everyone. 
- Moreover, if your programme is a Console-based (running from a Terminal or Command Prompt), then it should have a `static` method called `main`. Java interpreter will run this `main` method.
- To display to the screen we use the `println()` method which belongs to `System.out`. 
- Notice that Java uses semicolon, i.e. `;`, to end every statement.
- Unlike Python which uses whitespace to indicate a block of statement, Java uses curly braces `{}`. 

To run the code you have to do the following in your Terminal:
- Compile the `.java` source code into `.class` file. This is a binary file that can be read by Java Virtual Machine.
- Run the `.class` file using Java Runtime Environment.

To compile the code:
```sh
$ javac Hello.java
```
**Note that you need to name the file with the same name as your class name**.

To run the binary file:
```sh
$ java Hello
```

The argument after the `java` program is your class name `Hello`.

## Primitive Data Types

Whereas Python's numeric data type are mainly of integers or floats, Java has a number of different numeric data types. Java's Numeric Data types include:
- `byte`: 8-bit signed
- `short`: 16-bit signed
- `int`: 32-bit signed
- `long`: 64-bit signed
- `float`: 32-bit floating point IEEE 754
- `double`: 64-bit floating point IEEE 754

An integer literal is assumed to be `int` unless you end it with `L` to be a `long`. Similarly, for floating point literal, it is assumed to be a `double`. You can specify the particular type by adding either `F` or `D` at the end of the literal.

```java
// java

System.out.println(17); // this is an int
System.out.println(17L); // this is a long
System.out.println(17.); // this is a double
System.out.println(17.F); // this is a float
System.out.println(17.D); // this is a double
```

## String

As for String, Python has several ways of creating strings like:
```python
# python

print("17")
print('17')
print("""17""")
print('''17''')
```

On the other hand, Java uses only double quotes to create a String.

```java
// java

System.out.println("17");
```

To create other quotes in the string use the escape character back slash `\`.

```java
// java

System.out.println("\'17\'")
System.out.println("\"17\"")
```

Java reserves the single quotes for Character data type, i.e. `char`, which is different from String. Python, on the other hand, does not have a separate data type for Character.

```java
// java

System.out.println("Printing Character:" + 'A');
```

## Variable Declaration

Unlike Python, Java **requires you to declare** your variables before you can use them. To declare a variable, you need to follow the following syntax:

```java
type varName;
```

For example, you can declare an `int` variable called `age` as follows.
```java
// java

int age;
```
This variable declaration allows Java to allocate memory for this data and do some check when your program runs. Once a variable is declared, you can then use it either by *assigning* a value or by *accessing* its value.

```java
// java

age = 17; // assigning a value
System.out.println(age); // accessing the value
```

You can also declare a variable and initialize its value immediately. 

## Math Operators

Python and Java has similar math operators. The only different is that Python 3 onwards uses true division in operating two integers.

```python
# python

print(3/2) # this gives 1.5
```

On the other hand, Java's division depends on the data type of the two operands. If both are integers, it will perform integer division. However, if one of them is a floating point number, the result will be a floating point number.

```java
// java

System.out.println(3/2); // this results in int
System.out.println(3/2.0); // this results in double
System.out.println(3.0/2); // this results in double
```

In Python, you can print both string and numeric data types in the following ways:
```python
# python

print("3/2 = ", 3/2)
print("3/2 = " + str(3/2))
print("3/2 = {:d}".format(3/2))
```

In Java, you can use the `+` operator between String and any numeric data type.

```java
// java

System.out.println("3/2 = " + 3/2);
System.out.println("3/2 = " + 3/2.0);
```

Similar to Python, Java also has the following augmented assignment operators.

```java
// java

count += 1
count -= 1
count /= 1
count *= 1
```

Because Java in a way is inspired from C++, it also has the increment and decrement operators.

```java
// java

count++
count--
```
These operators increment or decrement the variable `count` by one. This operators can be a *prefix* or *postfix*. You will only see the difference if it is used with some other operators.

Let's see the difference between an *prefix* and *postfix* increment operator using the following example.

```java
// java

int i = 10;
int newNum = 10 * i++;
```
This is the same as
```java
// java

int i = 10;
int newNum = 10 * i;
i = i + 1;
```

On the other hand, the following *prefix* operator

```java
// java

int i = 10;
int newNum = 10 * (++i);
```
is the same as

```java
// java

int i = 10;
i = i + 1;
int newNum = 10 * i;
```

## Numeric Type Conversion

In Python, each built-in data type has its conversion function such as `int()`, `float()`, and `str()`. Java does something different to convert numeric data. In Java, it is called **type casting**. 

```java
// java

System.out.println((double) 3 / 2); // results in 1.5
```

In the above example, we cast `int` data of `3` to be double before performing the division by `2`. Since one of the operand is a `double`, the result is a `double`, i.e. `1.5`. 

## Converting String to Numbers

To do this, we call the method from the respective class `Integer` or `Double` called `parseInt()` or `parseDouble()`. 

```java
// java

System.out.println(Integer.parseInt("17"));
System.out.println(Double.parseDouble("17"));
```

## Reading Input from the Console

Python uses `input()` function to read the user's input from the keyboard. In Java, you have to use the `Scanner` class. There are several steps to do this:
- import `Scanner` from `java.util`
- create a new `Scanner` object
- use the right method to read the data, 
    - `nextByte()`
    - `nextShort()`
    - `nextInt()`
    - `nextLong()`
    - `nextFloat()`
    - `nextDouble()`
    - `next()`
    - `nextLine()`

The method `nextLine()` is similar to `input()` in Python because reads a string that ends with the Enter key pressed.  On the other hand, `next()` method reads a string that ends before a whitespace character. 

```java
// java

import java.util.Scanner;

public class TestScanner{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        String name;
        int age;
        System.out.print("Enter your name: ");
        name = input.nextLine();
        System.out.print("Enter your age: ");
        age = input.nextInt();
        System.out.println(name + " is " + age + " years old.");
    }
}
```

Note also the difference with Python's `input()`. In Python's `input()` the argument is the prompt. Java's Scanner's method has no argument. So you need to use the `print()` or `println()` method to display the prompt.

## Methods

Methods are similar to functions and are part of a class or an object. Since Java is pure object oriented, all functions are written as methods of a class. 

### Defining a Method

Recall that in Python, a function is defined as follows:

```python
# python

def foo(arg1, arg2):
    # Body
    print("Do something here.")
    return None
```

And when you create a class, the method in Python's class is defined similarly, with the first argument to be `self`. 

```python
# python

class Foo:
    def foo_method(self, arg1, arg2):
        print("This is doing nothing and returning nothing")
        return None
```

Java has the following syntax in defining the method:

```java
modifier returnValueType methodName(arguments){
    // body
}
```

For example, we can create a square method that squares two numbers as follows:

```java
// java

public class TestMethods{
    public static void main(String[] args){
        int val = 4;
        int result = square(val); // invoke a method

        System.out.printf("The square of %d is %d.\n", val, result);

    }

    // this is method definition
    public static int square(int n1){
        return n1 * n1;
    }
}
```

Note the following:
- We need to do the following when dealing with methods:
    - defining a method
    - invoking a method
- We use `printf()` function instead of `println` s that we can format the output using the format specifier `%d` for integer numbers. This is similar to Python's string formatting, e.g. `"The square of {:d} is {:d}".format(val, result)`.
- In the method definition, we observe the following:
    - The method's name is `square`. You need this when invoking a method.
    - There is only one input argument, which is `n1` and its type is `int`. 
    - The return value type is `int`.
    - It uses two modifiers `public` and `static`. For this short course, you will use only `public static` modifier. You will learn more of other modifiers in ISTD.

If your function does not have a return value, you should specify `void` as the return type. For example, to do something similar like the above Python's `foo` function, we can define it in Java as follows.

```java
// java

public static void fooMethod(int arg1, int arg2){
    // Body
    System.out.println("This method does not do anything and returns nothing.");
}
```

Recall that Python does not require you to declare the arguments' type. On the other hand, we have to declare all variables and arguments in Java. So in our `fooMethod()`, we still need to declare our arguments' type (in this case I just declare it as `int`) even though the arguments are not used.

### Passing Parameters by Values

When you invoke a method with an argument, the value of the argument is passed to the parameter. If the argument is of *primitive data type*, the argument is *passed-by-value*. This means that a copy of the value is passed and any changes inside the method will not affect the actual argument. We can see this for the following example.

We can print both the values of a variable before and after a call to a method.

```java
//java

int x = 1;
System.out.println("Before the call, x is " + x);
increment(x);
System.out.println("After the call, x is " + x);
```

The `increment()` method is defined as follows:
```java
// java

public static void increment(int n){
    n++;
    System.out.println("n inside the method is " + n);
}
```

Notice the output that the value of the argument inside the method is printed as `2`, but the value of `x` after the call remains as `1`. 

## Branch or Selection

All programming languages support selection to implement the branch structure. They do this by supporting Boolean data type as well as providing some construct to select a branch or path depending on some conditions.

### Boolean Data Type

In Python, Boolean data type is created either by assigning to `True` or `False` or through logical and relational operators. 

```python
# python

young = True
senior = age > 60
```
Both `young` and `senior` are Boolean data type. Python can infer the data type from the value assigned. In Java, you need to declare every Boolean variable as `boolean` data type.

```java
// java

boolean young = true;
int age = 17;
boolean senior = age > 60;
```

Notice that Java uses **small letter** `true` instead of `True` in Python. Similarly, Java uses **small letter** `false`. 

### Comparison Operators

Java has the same comparison operators as Python:
- `<`, `<=`, `>`, `>=`
- `==`, `!=`

### Logical Operators

However, Python and Java differs in their boolean logical operators.

| Python | Java |
|--------|------|
| and    | &&   |
| or     | \|\| |
| not    | !    |

Moreover, Python does not have xor logical operator, but Java has xor logical operator using `^` operator.

### If-Else Statement

The branch structure is implemented using the `if-else` statement. 

In Python, if we have a boolean variable called `condition`, we can write the if-else statement as follows.

```python
# python

if condition:
    # block A
    print("When condition is True.")    
else:
    # block B
    print("When condition is False.")   
# block C
print("Outside if-else, will be executed regardless of the condition.")
```

On the other hand, in Java:

```java
// java

if (condition){
    // block A
    System.out.println("When condition is True.");
} else{
    // block B
    System.out.println("When condition is False.");
}
// block C
System.out.println("Outside if-else, will be executed regardless of the condition.");
```
Note the following:
- you need to use `()` for your condition.
- the block is identified using `{}` instead of indentation as in Python.
- block C will always be executed regardless the condition because it is the next statement after the if-else statement.

If you have more than one conditions to check, Python has `if-elif-else` statement.

```python
# python
if condition_1:
    # block A
    print("When condition_1 is True.")
elif condition_2:
    # block B
    print("When condition_1 is False and condition_2 is True.")
else:
    # block C
    print("When none of the conditions above are True.")
# block D
print("This is a next statement after the if-else statement.")
```

Java does not have an `elif` keyword. Instead, it makes use the existing `else` and `if` keyword as shown below.

```java
// java

if (condition1){
    // block A
    System.out.println("When condition1 is True.");
} else if (condition2){
    // block B
    System.out.println("When condition1 is False and condition2 is True.");
} else{
    // block C
    System.out.println("When none of the conditions above are True.");
}
// block D
System.out.println("This is a next statement after the if-else statement.");
```

Besides of using Boolean variable, you can also write directly the Boolean expression into the condition part of the if-else statement.

### Switch Statement
Java has another construct to implement selection which Python does not have, i.e. the `switch` statement. When you have a lot of conditions, `switch` statement maybe neater to read.

```java
// java

switch (expression){
    case value1: statements1;
                 break;
    case value2: statements2;
                 break;
    case value3: statements3;
                 break;
    case value4: statements4;
                 break;
    default: statements5;
}
```

An example of this would be printing different error HTML codes as follows:

```java
//java

switch (code){
    case 100:   System.out.println("Continue.");
                break;
    case 200:   System.out.println("OK.");
                break;
    case 201:   System.out.println("Created.");
                break;
    case 202:   System.out.println("Accepted.");
                break;
    case 400:   System.out.println("Bad Request.");
                break;
    case 401:   System.out.println("Unauthorized.");
                break;
    case 403:   System.out.println("Forbidden.");
                break;
    case 404:   System.out.println("Not Found.");
                break;
    case 500:   System.out.println("Server Error.");
                break;
}
```

Note that the `switch` statement observes the following rules:
- The expression after the `switch` statement must result in either `char`, `byte`, `short`, `int`, or `String` data type.
- The value after the keyword `case` must have the same data type as the result expression.
- When the value in the `case` matches, the statement inside the case will be executed **until** either:
    - a break statement is encountered, or
    - the end of the `switch` statement. This means that if you do not have the `break` statement, it will continue to execute the subsequent statements in the next `case` block.
- The `break` statement is optional, but in most cases, you do want to use it. See the previous point explanation.
- The `default` case is optional and can be used to execute some codes when none of the cases matches.



## Some Useful Classes

You can use `Math` class to use some of its methods. You can refer to [TestLib.java](Lesson01/TestLib.java) for some example code, but do refer to the [Math Class documentation](https://docs.oracle.com/javase/8/docs/api/java/lang/Math.html) for more detail.
