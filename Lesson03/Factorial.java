public class Factorial{
	public static void main(String[] args){
		System.out.println(factorialRecursion(5));
		System.out.println(factorialLoop(5));
	}

	public static int factorialRecursion(int n){
		if (n == 0){
			return 1;
		} else{
			return n * factorialRecursion(n - 1);
		}
	}

	public static int factorialLoop(int n){
		if (n == 0){
			return 1;
		} else{
			int result = 1;
			for (int number = 1; number <= n; number++){
				result *= number;
			}
			return result;
		}
	}
}