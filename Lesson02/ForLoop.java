public class ForLoop{
	public static void main(String[] args){
		int[] numbers = {1, 2, 3, 4, 5};

		for (int idx = 0; idx < numbers.length; idx++){
			System.out.println(numbers[idx]);
		}
		// System.out.println(idx);
	}
}