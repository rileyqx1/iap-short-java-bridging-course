import java.util.Scanner;

public class SumWhile{
	public static void main(String[] args){
		int data;
		int sum = 0;

		Scanner input = new Scanner(System.in);

		System.out.print("Enter a positive number to add, end with -1: ");
		data = input.nextInt();

		while (data != -1){
			sum += data;

			System.out.print("Enter a positive number to add, end with -1: ");
			data = input.nextInt();
		} 

		System.out.println("The sum is " + sum);

	}
}