import java.util.Scanner;

public class SumDoWhile{
	public static void main(String[] args){
		int data;
		int sum = 0;

		Scanner input = new Scanner(System.in);

		do{
			System.out.print("Enter a positive number to add, end with -1: ");
			data = input.nextInt();
			sum += data;
		} while (data != -1);

		sum++;
		System.out.println("The sum is " + sum);

	}
}