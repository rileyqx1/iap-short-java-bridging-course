# Recursion
## Learning Objectives

By the end, students should be able to:
- break problems using recursion
- identify base case and recursive case
- write recursive method in Java
- write a helper method for recursion

## Introduction

Recursion is a technique that provides elegant solutions to problems that are difficult to program using simple loops. We will start with some examples that both simple loops and recursion can do and then we will move to those which recursion is more suitable.

To use recursion is to program using *recursive methods*. This means that we will use methods that invoke themselves. Let's start with a simple example

## Summing Numbers

We have seen how we can use loops to sum elements in an array. 

```java
// java
public static int sumUsingForLoop(int[] numbers){
	int sum = 0

	for (int item: numbers){
		sum += item;
	}
	return sum;
}
```

How can we change this to use recursion? The idea of recursion is to break down the problem into smaller problems until it becomes trivial. For example, to sum four numbers say `1, 2, 3, 4`, we can break the problems into summing only two numbers:

```math
1 + sumNumbers(2, 3, 4) 
```
that is to sum 1 and the other number from the output of the `sumNumbers(2,3,4)`. We can also sum the three numbers `sumNumbers(2, 3, 4)` by summing only two numbers:

```math
2 + sumNumbers(3, 4)
```

Similarly, we can compute `sumNumbers(3, 4)` by summing only two numbers:

```math
3 + sumNumbers(4)
```

Now we arrive at only summing one number and this is trivial because the output of `sumNumbers(4)` is simply
```math
4
```

Now to get the final output, we have to go back **up**.

```math
3 + 4 \rightarrow 7
```

And we continue to

```math
2 + 7 \rightarrow 9
```
Similarly, we have

```math
1 + 9 \rightarrow 10
```
And now we have found the answer. 

In every recursion, we have to identify **two cases**:
1. Base Case: this is the case at the bottom of the recursion and the solution will start to move up. In our case, this is when we have left with only one number to sum where the output is trivial, which is that number. 
2. Recursive Case: this is the case when we repeat the program by calling itself. In the above example, this happens when there are more than one number to sum, e.g. $`2 + sumNumbers(3, 4)`$.

We can write the code as follows.

```java
// java

public static int sumUsingRecursion(int[] numbers){
	if numbers.length == 1{
		// this is the Base Case when there is only one number left
		return numbers[0];
	} else{
		// this is the Recursive Case when there is more than one elements
		return numbers[0] + sumUsingRecursion(numbers);
	}
}
```

## Factorial

Computing factorial is another example where we can use either loops or recursion. The factorial of a number $`n`$ can be defined as follows.

```math
0! = 1 \\
n! = n \times (n-1)!; n > 0
```

The above mathematical expression, in fact, is recursive. The reason is that to define $`n!`$, we actually make use of the factorial itself in $`(n-1)!`$. We can then easily identify the base case and the recursive case.
1. Base Case: when $`n = 0`$, the factorial is defined as 1.
1. Recursive Case: when $`n>0`$, the factorial is defined as $`(n-1)!`$.

We can write the Java method's implementation as follows:

```java
// java

public static int factorialRecursion(int n){
	if (n == 0){
		return 1;
	} else{
		return n * factorialRecursion(n - 1);
	}
}
```

This is a much more elegant solution as compared to if we implement using Loops.

```java

public static int factorialLoop(int n){
	if (n == 0){
		return 1;
	} else{
		int result = 1;
		for (int number = 1; number <= n; number++){
			result *= number;
		}
		return result;
	}
}
```

## Recursion with Helper Methods

In some cases, creating an additional helper method in solving recursion makes the code more efficient. Let's look at the Palindrome problem. Recall that a string is palindrome if it reads the same from the left and from the right. For example, "mom" and "dad" are palindromes, but "uncle" and "aunt" are not. 

We can divide this checking problem into two sub problems:
- Check whether the first character is the same as the last character.
- Ignore the two ends and check whether the rest of the substring in the middle is a palindrome.

We reach the base case when there is only one character left. In this case, the solution is trivial. If there is only one character, it is a palindrome. 

We can solve the recursive part of this problem by copying the original string into its substring using `arraycopy`. However, for a long string, this will create a long of copies of strings. Another way rather than copying the string is actually to pass the original string and use the indexes to point which character should be compared instead. Let's see the solution to this probem.

```java
// java

public static boolean isPalindrome(String s, int left, int right){
	if (left >= right){
		return true;
	} else if (s.charAt(left) != s.charAt(right)){
		return false;
	} else{
		return isPalindromeHelper(s, left + 1, right - 1);
	}
}
```

Notice that the first `if` condition is the base case. This happens when the indexes of the left and the right characters are at the middle position, i.e. ` low == high`, or when `low > high`. The case of `low > high` happens when there are even number of characters and there is no middle character. The other base case is when we found two characters that are not the same.  This is the second `if` condition. In this case, we need not check the rest of the characters in the middle since we know it is not a palindrome. The last `else` body is for the recursive case. In this case, we pass the original string, but we move our index left one position to the right, and our index right one position to the left. 

To use this method, we have to pass on the `left` and the `right` index into the argument. For the first call, these indexes are always at position `0` and `s.length()-1`. Since, it's always the same, it is convenient to create a *helper* method to call this recursive method for the first time.

```java
// java

public static boolean isPalindromeHelper(String s){
	return isPalindrome(s, 0, s.length() - 1);
}
```

In this way, one can simply call `isPalindromeHelper(s)` and pass on the string.

## Towers of Hanoi

The towers of Hanoi is a classic problem that can be solved easily using recursion but it is difficult to solve using loops. The problem involves moving a specified number of disks of distinct sizes from one tower to another while observing the following rules:
- There are *n* disks labeled 1, 2, 3, ... *n* and three towers A, B, and C.
- No disk can be on top of a smaller disk at any time.
- All disks are initially placed on tower A.
- Only one disk can be moved at a time, and it must be the smallest disk on that particular tower, i.e. the top disk. 
- The objective is to move all the disks from A to B with the assistance of C. 

How do we solve this problem using recursion? The key in every recursion is to identify the base case and the recursive case. In this case, we have the following:
- Base case: when there is only one tower, i.e. *n = 1*. In this case, we just move that one disk from A to B.
- Recursive case: when *n > 1*, we solve the problem in this way. Divide the disks into two parts. The first one is the last disk at the bottom. Let's call this *bottom disk*. The second one is the remaining *n-1* disks on the top of the bottom disk. Let's call this the *remaining disks*. We solve this problem by doing the following.
    - Move the *remaining disks* from A to C.
    - Move the *bottom disk* from A to B.
    - Move the *remaining disks* from C to B. 

Note that in the recursive case, we can do the steps in moving the *remaining disks* using the same steps by dividing the *remaining disks* into two parts, the bottom disk and the *other remaining disks*. 

Try to implement the Tower of Hanoi solution!

