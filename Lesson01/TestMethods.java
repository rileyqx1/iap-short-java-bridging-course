public class TestMethods{
	public static void main(String[] args){
		int val = 4;

		int result = square(val); // invoke a method

		System.out.printf("The square of %d is %d.\n", val, result);

		// Testing pass-by-value
		int x = 1;
		System.out.println("Before the call, x is " + x);
		increment(x);
		System.out.println("After the call, x is " + x);

	}

	// this is a method definition
	public static int square(int n1){
		return n1 * n1;
	}


	public static void fooMethod(int arg1, int arg2){
		// Body
		System.out.println("This method does not do anything and returns nothing.");
	}

	public static void increment(int n){
		n++;
		System.out.println("n inside the method is " + n);
	}
}