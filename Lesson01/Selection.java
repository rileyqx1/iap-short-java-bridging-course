public class Selection{
	public static void main(String[] args){
		//Boolean data type
		boolean young = true;
		int age = 17;
		boolean senior = age > 60;

		System.out.println(young);
		System.out.println(senior);

		// if-else statement
		if (senior){
			System.out.println("You are a senior citizen.");
		} else{
			System.out.println("You are not a senior citizen.");
		}
		System.out.println("This is always printed.");

		// if-else multiple condition statement
		if (senior){
			System.out.println("You are a senior citizen.");
		} else if (young){
			System.out.println("You are young!");
		} else{
			System.out.println("You are neither a senior citizen nor young, what are you?");
		}
		System.out.println("This is always printed.");

		// Switch statement
		int code = 400;
		switch (code){
			case 100: System.out.println("Continue.");
					  break;
			case 200: System.out.println("OK.");
					  break;
			case 201: System.out.println("Created.");
					  break;
			case 202: System.out.println("Accepted.");
					  break;
			case 400: System.out.println("Bad Request.");
					  break;
			case 401: System.out.println("Unauthorized.");
					  break;
			case 403: System.out.println("Forbidden.");
					  break;
			case 404: System.out.println("Not Found.");
					  break;
			case 500: System.out.println("Server Error.");
					  break;
		}
	}
}